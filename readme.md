# Leeloo LXP In Progress Courses Block
THis block allows admin to show inprogess courses of a user.

In prigress means the courses user is enrolled and the progress is greater then 0% and less than 100%
Installation Instructions
=========================

* Make sure you have all the required versions.
* Download and unpack the block folder.
* Place the folder (eg "tb_in_courses") in the "blocks" subdirectory.
* Visit http://yoursite.com/admin to complete the installation
* Turn editing on the my page.
* Add the block to the page ("Leeloo LXP In Progress Courses Block (tb_in_courses)")
* Visit the config link in the block for more options.

Moodle compatibility
=====================
* Tested with Moodle 3.1, 3.2, 3.3 and 3.4

License
=====================

GPL 3 or later